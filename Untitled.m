close all;clear all;clc;
 addpath('D:\mdz_win\LOCALIT\freesurfermatlab');

mriData = load_nifti('D:\mdz_win\LOCALIT\001.nii');
vol = mriData.vol;
vol_size = size(vol);
for i = 1:vol_size(3)
vol_sagittal(:,:,i) = imrotate(vol(:,:,i),270); % Sagittal View
end
for j = 1:vol_size(2)
vol_axial(:,:,j) = imrotate(vol(:,j,:),270); % Axial View
end
for k = 1:vol_size(1)
vol_coronal(:,:,k) = imrotate(vol(k,:,:),270); % Axial View
end


imshow(vol_coronal(:,:,100),[])