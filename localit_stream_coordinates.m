%%
%LOCALIT Stream into MATLAB: TCP/IP 6666
function localit_stream_coordinates
%%%%% make sure the instrument control toolbox is installed

interfaceObject = tcpip('172.20.127.143',6666); % IP address of Localite system with the transmitting JSON port
bytesToRead = 500;

interfaceObject.BytesAvailableFcn = {@localRead,bytesToRead};
interfaceObject.BytesAvailableFcnMode = 'byte';
interfaceObject.BytesAvailableFcnCount = bytesToRead;

fopen(interfaceObject);
pause(3);
snapnow;
function localRead(interfaceObject,~,bytesToRead)
data = fread(interfaceObject,bytesToRead);
showtext = char(data');

k_coil_text1 = extractBetween(showtext,'"coil_1_position": {','}}{"');




