close all; clc; clear all
data_orig_MRI = load_nifti('D:\mdz_win\LOCALIT\001.nii');
load etot.mat
outfile = 'eabs_all_mgh_1001.nii';
nii_tmp = data_orig_MRI;
nii_tmp.vol = sqrt(Ex.^2+Ey.^2+Ez.^2);
wer = save_nifti(nii_tmp,outfile);
