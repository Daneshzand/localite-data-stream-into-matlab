%close all; clc; clear all;
dataorg = load_nifti('ANorig.nii');
triggermarkers = fileread('triggermarkers.txt');
data00 = extractBetween(triggermarkers,'data00="','"');
data01 = extractBetween(triggermarkers,'data01="','"');
data02 = extractBetween(triggermarkers,'data02="','"');
data03 = extractBetween(triggermarkers,'data03="','"');
data10 = extractBetween(triggermarkers,'data10="','"');
data11 = extractBetween(triggermarkers,'data11="','"');
data12 = extractBetween(triggermarkers,'data12="','"');
data13 = extractBetween(triggermarkers,'data13="','"');
data20 = extractBetween(triggermarkers,'data20="','"');
data21 = extractBetween(triggermarkers,'data21="','"');
data22 = extractBetween(triggermarkers,'data22="','"');
data23 = extractBetween(triggermarkers,'data23="','"');
data30 = extractBetween(triggermarkers,'data30="','"');
data31 = extractBetween(triggermarkers,'data31="','"');
data32 = extractBetween(triggermarkers,'data32="','"');
data33 = extractBetween(triggermarkers,'data33="','"');

for i = 1:length(data00)
    C(:,:,i) = [str2double(char(data00(i))) str2double(char(data01(i))) str2double(char(data02(i))) str2double(char(data03(i)))
        str2double(char(data10(i))) str2double(char(data11(i))) str2double(char(data12(i))) str2double(char(data13(i)))
        str2double(char(data20(i))) str2double(char(data21(i))) str2double(char(data22(i))) str2double(char(data23(i)))
        str2double(char(data30(i))) str2double(char(data31(i))) str2double(char(data32(i))) str2double(char(data33(i)))];
end

affine = C(:,:,1);
affineT = affine(1:3,4)';
Sx = sqrt(sum(affine(1,1:3).^2));
Sy = sqrt(sum(affine(2,1:3).^2));
Sz = sqrt(sum(affine(3,1:3).^2));
affineR = zeros(4,4); affineR(4,4) = 1;
affineR(1,1:3) = affine(1,1:3)/Sx;
affineR(2,1:3) = affine(2,1:3)/Sy;
affineR(3,1:3) = affine(3,1:3)/Sz;
angle = acos(( affine(1,1) + affine(2,2) + affine(3,3)-1)/2)
x_ang = (affineR(3,2)-affineR(2,3))/sqrt((affineR(3,2)-affineR(2,3))^2+(affineR(1,3)-affineR(3,1))^2+(affineR(2,1)-affineR(1,2))^2);
y_ang = (affineR(1,3)-affineR(3,1))/sqrt((affineR(3,2)-affineR(2,3))^2+(affineR(1,3)-affineR(3,1))^2+(affineR(2,1)-affineR(1,2))^2);
z_ang = (affineR(2,1)-affineR(1,2))/sqrt((affineR(3,2)-affineR(2,3))^2+(affineR(1,3)-affineR(3,1))^2+(affineR(2,1)-affineR(1,2))^2);
save D:\mdz_win\TMS_Calc\test\RT x_ang y_ang z_ang affineT affineR
